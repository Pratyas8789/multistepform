import React from 'react'
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux';
import { handleSignupData, handleStep, validate } from '../Redux/reducer';

export default function SignUpContainer() {
    const dispatch = useDispatch()
    const { firstName, lastName, dob, email, address, error, isSubmit } = useSelector(store => store.signup)

    const handleChange = (e) => {
        const { name, value } = e.target
        dispatch(handleSignupData({ name: name, value: value }))
    }
    const handleCheck = (e) => {
        e.preventDefault()
        dispatch(validate())
    }
    if (isSubmit && Object.keys(error).length == 0) {
        dispatch(handleStep(2))
    }

    return (
        <div>
            <h1>Sign up</h1>
            <Form className='w-100 h-75' onSubmit={handleCheck} >
                <Row className="">
                    <Form.Group as={Col} md="4" controlId="validationCustom01"
                        className='signupRow'>
                        {error.firstName ?
                            <div>
                                <Form.Label className='text-danger' >First name</Form.Label>
                                <Form.Control
                                    name='firstName'
                                    type="text"
                                    value={firstName}
                                    placeholder="First name"
                                    onChange={handleChange}
                                    className='border border-danger'
                                />
                                <p className='text-danger' >
                                    {error.firstName}
                                </p>
                            </div> :
                            <div>
                                <Form.Label>First name</Form.Label>
                                <Form.Control
                                    name='firstName'
                                    type="text"
                                    value={firstName}
                                    placeholder="First name"
                                    onChange={handleChange}
                                />
                                <p className='text-danger' >
                                    {error.firstName}
                                </p>
                            </div>}
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        {error.lastName ?
                            <>
                                <Form.Label className='text-danger' >Last name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Last name"
                                    name='lastName'
                                    value={lastName}
                                    onChange={handleChange}
                                    className='border border-danger'
                                />
                                <p className='text-danger' >
                                    {error.lastName}
                                </p> </> :
                            <><Form.Label>Last name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Last name"
                                    name='lastName'
                                    value={lastName}
                                    onChange={handleChange}
                                />
                                <p className='text-danger' >
                                    {error.lastName}
                                </p></>}
                    </Form.Group>
                </Row>
                <Row className="signupRow">
                    <Form.Group as={Col} md="4" controlId="validationCustom01">
                        {error.dob ?
                            <>
                                <Form.Label className='text-danger'>Date of birth</Form.Label>
                                <Form.Control
                                    type="date"
                                    name='dob'
                                    value={dob}
                                    onChange={handleChange}
                                    className='border border-danger'
                                />
                                <p className='text-danger' >
                                    {error.dob}
                                </p></> : <>
                                <Form.Label >Date of birth</Form.Label>
                                <Form.Control
                                    type="date"
                                    name='dob'
                                    value={dob}
                                    onChange={handleChange}
                                />
                                <p className='text-danger' >
                                    {error.dob}
                                </p>
                            </>}
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom02"
                        className='signupRow'>
                        {error.email ?
                            <>
                                <Form.Label className='text-danger'>Email address</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="email"
                                    name='email'
                                    value={email}
                                    onChange={handleChange}
                                    className='border border-danger'
                                />
                                <p className='text-danger' >
                                    {error.email}
                                </p></>
                            : <>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="email"
                                    name='email'
                                    value={email}
                                    onChange={handleChange}
                                />
                                <p className='text-danger' >
                                    {error.email}
                                </p></>}
                    </Form.Group>
                </Row>
                <Form.Group className="signupRow">
                    {error.address ?
                        <>
                            <Form.Label className='text-danger'>Address</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="address"
                                name='address'
                                value={address}
                                onChange={handleChange}
                                className='border border-danger'
                            />
                            <p className='text-danger' >
                                {error.address}
                            </p></> : <>
                            <Form.Label>Address</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="address"
                                name='address'
                                value={address}
                                onChange={handleChange}
                            />
                            <p className='text-danger' >
                                {error.address}
                            </p></>}
                </Form.Group>
                <hr />
                <div className='d-flex justify-content-end'>
                    <Button className='' type="submit">Next step</Button>
                </div>
            </Form>
        </div>
    )
}