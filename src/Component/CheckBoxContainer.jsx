import React from 'react'
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import { useDispatch, useSelector } from 'react-redux';
import { handleCheckboxOption, handleStep, handleToogle, validate } from '../Redux/reducer';

export default function CheckBoxContainer() {
    const { checkboxSubmit, error, toogle, checkboxOption } = useSelector(store => store.signup)
    const dispatch = useDispatch()
    const handleClick = (e) => {
        dispatch(handleToogle(e.target.id))
    }
    const handleSubmit = () => {
        dispatch(validate())
    }
    const handleOption = (e) => {
        dispatch(handleCheckboxOption(e.target.id))
    }
    return (
        <>
            {(checkboxSubmit && Object.keys(error).length === 0) ? <h1>Your form is successfully submitted !!!</h1> :
                <div className='w-100 h-100'>
                    <div
                        className='checkBoxContainer'>
                        <div className=' checkBoxImageContainer'>
                            <h1>Checkbox</h1>
                            <div className='d-flex w-100 ' >
                                <button className=' image border-0 me-2 w-50 '
                                    onClick={handleClick}>
                                    <Image className={`w-100 h-100 border ${toogle == 1 ? "border-primary" : "border-white"}`} id='1' src="./image/boy.png" rounded />
                                </button>
                                <button className=' image border-0 me-2 w-50 '
                                    onClick={handleClick}>
                                    <Image className={`w-100 h-100 border ${toogle == 2 ? "border-primary" : "border-white"}`} id='2' src="./image/girl.png" rounded />
                                </button>
                            </div>
                            <p className='text-danger' >
                                {error.toogle}
                            </p>
                        </div>

                        <div
                            className='checkboxOptionContainer'>
                            <div className='d-flex '>
                                <input type="radio" name="a" id="1"
                                    onClick={handleOption}
                                    checked={checkboxOption == 1}
                                />
                                <p className=' mt-3 ms-2' >I want to add this option.</p>
                            </div>
                            <div className='d-flex '>
                                <input type="radio" name="a" id="2"
                                    onClick={handleOption}
                                    checked={checkboxOption == 2}
                                />
                                <p className=' mt-3 ms-2' >Let me click on this checkbox and choose some cool stuf.</p>
                            </div>
                            <p className='text-danger' >
                                {error.checkboxOption}
                            </p>
                        </div>
                    </div>
                    <div className='checkBoxButonContainer d-flex justify-content-end mt-1'>
                        <Button onClick={() => dispatch(handleStep(2))} variant="light">Back</Button>{' '}
                        <Button className='ms-5' variant="primary" onClick={() => { handleSubmit() }}>Submit</Button>{' '}
                    </div>
                </div>
            }
        </>
    )
}