import React from 'react'
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux';
import { handleMessageChoice, handleMessageData, handleStep, validate } from '../Redux/reducer';

export default function MessageContainer() {
    const { message, messageSubmit, error, messageChoice } = useSelector(store => store.signup)
    const dispatch = useDispatch()
    const handleChange = (e) => {
        const { name, value } = e.target
        dispatch(handleMessageData({ name: name, value: value }))
    }
    const handleNext = () => {
        dispatch(validate())
    }
    const handleClick = (e) => {
        dispatch(handleMessageChoice(e.target.id))
    }
    if (messageSubmit && Object.keys(error).length === 0) {
        dispatch(handleStep(3))
    }
    return (
        <div>
            <h1>Message</h1>
            <p>Message</p>
            <div className='messageDiv'>
                <textarea name="message" id="" value={message} cols="60" rows="6" onChange={handleChange} ></textarea>
                <br />
                <p className='text-danger' >
                    {error.message}
                </p>
            </div>
            <div
                className='messageChoice'>
                <div className='d-flex'  >
                    <div className='d-flex align-items-center h-100 w-25' >
                        <input type="radio" name="a" id="1"
                            onClick={handleClick} checked={messageChoice == 1}
                        />
                        <p className=' mt-3 ms-2'>The number one choice</p>
                    </div>
                    <div className='d-flex '>
                        <input type="radio" name="a" id="2" onClick={handleClick}
                            checked={messageChoice == 2}
                        />
                        <p className=' mt-3 ms-2' >The number two choices</p>
                    </div>
                </div>
                <p className='text-danger' >
                    {error.messageChoice}
                </p>
            </div>
            <hr />
            <div className='d-flex justify-content-end mt-3' >
                <Button onClick={() => dispatch(handleStep(1))} variant="light">Back</Button>{' '}
                <Button onClick={handleNext} className='ms-5' variant="primary">Next step</Button>{' '}
            </div>
        </div>
    )
}