import React from 'react'
import Image from 'react-bootstrap/Image';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import CheckBoxContainer from './CheckBoxContainer';
import { useSelector } from 'react-redux';
import SignUpContainer from './SignUpContiner';
import MessageContainer from './MessageContainer';
import { TiTick } from 'react-icons/ti';

export default function Signup() {
    const { step, checkboxSubmit, error } = useSelector(store => store.signup)
    return (
        <div className='d-flex  h-75 w-75'  >
            <div className='h-100 w-25' >
                <Image className='w-100 h-100' src="./image/signup.png" rounded />
            </div>
            <div className='d-flex flex-column h-100 w-75 ps-5 ' >
                <Navbar className='bg-white border-bottom'>
                    <Container>
                        <Nav className="me-auto">
                            {step === 1 && <Nav.Link ><Button variant="primary">1</Button>{' '}Signup</Nav.Link>}
                            {step > 1 && <Nav.Link ><Button variant="light"><TiTick /></Button>{' '}Signup</Nav.Link>}

                            {step === 2 && <Nav.Link ><Button variant="primary">2</Button>{' '}Message</Nav.Link>}
                            {step === 1 && <Nav.Link ><Button variant="light">2</Button>{' '}Message</Nav.Link>}
                            {step === 3 && <Nav.Link ><Button variant="light"><TiTick /></Button>{' '}Message</Nav.Link>}

                            {step < 3 && <Nav.Link ><Button variant="light">3</Button>{' '}Checkbox</Nav.Link>}
                            {checkboxSubmit && Object.keys(error).length === 0 ? <Nav.Link ><Button variant="light"><TiTick /></Button>{' '}Checkbox</Nav.Link> : step === 3 && <Nav.Link ><Button variant="primary">3</Button>{' '}Checkbox</Nav.Link>}
                        </Nav>
                        <hr />
                    </Container>
                </Navbar>
                <div className='d-flex flex-column h-75 pt-5 '   >
                    <p>step {step}/3 </p>
                    {step === 1 && <SignUpContainer />}
                    {step == 2 && <MessageContainer />}
                    {step === 3 && <CheckBoxContainer />}
                </div>
            </div>
        </div>
    )
}
