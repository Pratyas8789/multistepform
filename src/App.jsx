import './App.css'
import Signup from './Component/Signup'

function App() {
  return (
    <div className='App d-flex justify-content-center align-items-center' >
      <Signup/>
    </div>
  )
}
export default App