import { configureStore } from '@reduxjs/toolkit'
import signupReducer from './reducer'

export const store = configureStore({
    reducer: {
        signup: signupReducer
    }
})