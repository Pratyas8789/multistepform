import { createSlice } from '@reduxjs/toolkit'
import { } from 'react-redux'

const initialState = {
    firstName: "",
    lastName: "",
    dob: "",
    email: "",
    address: "",
    message: "",
    messageBullet: false,
    checkBox: false,
    checkBoxBullet: false,
    error: {},
    step: 1,
    isSubmit: false,
    messageSubmit: false,
    checkboxSubmit: false,
    toogle: -1,
    messageChoice: -1,
    checkboxOption: -1,
}
const signupSlice = createSlice({
    name: "signupPage",
    initialState,
    reducers: {
        handleSignupData: (state, { payload }) => {
            state[payload.name] = payload.value
        },
        handleStep: (state, { payload }) => {
            state.isSubmit = false
            state.messageSubmit = false
            state.messageBullet = false
            state.checkBoxBullet = false
            state.checkboxSubmit = false

            state.step = payload
        },
        handleMessageData: (state, { payload }) => {
            state[payload.name] = payload.value
        },
        handleMessageChoice: (state, { payload }) => {
            state.messageChoice = payload
        },
        handleCheckboxOption: (state, { payload }) => {
            state.checkboxOption = payload

        },
        handleToogle: (state, { payload }) => {
            state.toogle = payload
        },
        validate: (state) => {
            let { firstName, lastName, dob, email, address, step, message, messageBullet, toogle, checkBoxBullet, messageChoice, checkboxOption } = state
            state.isSubmit = true
            const regex = (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)

            if (step === 1) {
                state.error = {}
                if (!firstName) {
                    state.error["firstName"] = "firstname is required !"
                } else {
                    if (state.error.hasOwnProperty('firstName')) {
                        delete state.error.firstName
                    }
                }

                if (!lastName) {
                    state.error["lastName"] = "lastName is required !"
                } else {
                    if (state.error.hasOwnProperty('lastName')) {
                        delete state.error.lastName
                    }
                }

                if (!dob) {
                    state.error["dob"] = "dob is required !"
                } else {
                    if (state.error.hasOwnProperty('dob')) {
                        delete state.error.dob
                    }
                }

                if (!email) {
                    state.error["email"] = "email is required !"
                } else if (!regex.test(email)) {
                    state.error["email"] = "please enter valid email !"
                }
                else {
                    if (state.error.hasOwnProperty('email')) {
                        delete state.error.email
                    }
                }

                if (!address) {
                    state.error["address"] = "address is required !"
                } else {
                    if (state.error.hasOwnProperty('address')) {
                        delete state.error.address
                    }
                }
            }

            if (step == 2) {
                state.error = {}
                state.messageSubmit = true
                if (!message) {
                    state.error["message"] = "message is required !"
                } else {
                    delete state.error.message
                }

                if (messageChoice == -1) {
                    state.error['messageChoice'] = "please select one option !"
                } else {
                    delete state.error.messageChoice
                }
            }

            if (step == 3) {

                state.error = {}
                state.checkboxSubmit = true
                if (toogle === -1) {
                    state.error['toogle'] = "checkbox is required !"
                } else {
                    delete state.error.toogle
                }
                if (checkboxOption == -1) {
                    state.error['checkboxOption'] = "please select one option !"
                } else {
                    delete state.error.checkboxOption
                }
            }
        }
    }
})

export const { handleMessageChoice, handleCheckboxOption, handleToogle, handleSignupData, validate, handleStep, handleCheckBoxBullet, handleMessageData, handleMessageBullet, checkboxOption } = signupSlice.actions
export default signupSlice.reducer